package com.taskfortesters.main;

public class Calculator {

    public double getSum(double a,double b){
        return a+b;
    }

    public double getDifference(double a,double b){
        return a-b;
    }

    public double getMultiplication(double a,double b){
        return a*b;
    }

    public double getQuotient(double a,double b){

        if (b==0){
            return 0;
        }
        return a/b;
    }

}