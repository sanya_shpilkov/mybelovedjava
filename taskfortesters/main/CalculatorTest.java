package com.taskfortesters.main;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.Random;
import static org.testng.Assert.*;

public class CalculatorTest {

    Calculator calc;
    Random random;
    double a;
    double b;

    @BeforeMethod
    public void setUp() {
        calc = new Calculator();
        random = new Random();
        a = random.nextDouble(100);
        b = random.nextDouble(100);
        System.out.println("setUp");
    }

    @AfterMethod
    public void tearDown() {
        System.out.println("tearDown");
    }

    @Test
    public void testGetSum() {

        double actual=calc.getSum(a,b);
        double expected=a+b;
        assertEquals(actual,expected);
    }
 
    @Test
    public void testGetDifference() {

        double actual=calc.getDifference(a,b);
        double expected=a-b;
        assertEquals(actual,expected);
    }

    @Test
    public void testGetMultiplication() {

        double actual=calc.getMultiplication(a,b);
        double expected=a*b;
        assertEquals(actual,expected);
    }

    @Test
    public void testGetQuotient() {

        double actual=calc.getQuotient(a,b);
        double expected=0;
        if (b!=0) expected=a/b;
             
        assertEquals(actual,expected);
    }
}